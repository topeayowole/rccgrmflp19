@if (session('success'))
    {{-- <style>
        .text{
            color: white; 
            display: inline-block; 
            margin-right: 10px;
        }
    </style>

    <div style="padding: 20px 30px; background: rgb(0, 166, 90) none repeat scroll 0% 0%; z-index: 999999; font-size: 16px; font-weight: 600;">
        <a class="pull-right" href="#" data-toggle="tooltip" data-placement="left" style="color: rgb(255, 255, 255); font-size: 20px;">×</a>
        <p style="color:#fff;">{{ session('success')}}</p>
    </div> --}}

    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        {{ session('success')}}
    </div>
@endif
    

