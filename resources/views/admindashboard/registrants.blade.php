@extends('layouts.dashboard')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registrants
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid" id="app">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Registrants
            </h3>
          </div>
          <div class="box-body">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Phone</th>
                            <th>WhatsApp Phone</th>
                            <th>Email</th>
                            <th>Age Range</th>
                            <th>Profession</th>
                            <th>RCCG/Non-RCCG</th>
                            <th>If RCCG LP19</th>
                            <th>Date Registered</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($registrants as $registrant)
                            <tr>
                                <td><p>{{ $registrant->id }}</p></td>
                                <td><p>{{ $registrant->fullname }}</p></td>
                                <td><p>{{ $registrant->phone}}</p></td>
                                <td><p>{{ $registrant->whatsapp_phone}}</p></td>
                                <td><p>{{ $registrant->email}}</p></td>
                                <td><p>{{ $registrant->age_range}}</p></td>
                                <td><p>{{ $registrant->occupation}}</p></td>
                                <td><p>{{ $registrant->rccg_non_rccg}}</p></td>
                                <td><p>{{ $registrant->if_rccg_lp19}}</p></td>
                                <td><p>{{ $registrant->created_at->format('jS F, Y')}}</p></td>
                            </tr> 
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </section>
@endsection

@section('scripts')
   <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
   <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
   <script>
       $('#example1').DataTable({
           "aaSorting": []
       });
   </script> 
@endsection