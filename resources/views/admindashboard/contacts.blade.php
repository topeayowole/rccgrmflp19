@extends('layouts.dashboard')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Contacts
      </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid" id="app">
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Contacts
            </h3>
          </div>
          <div class="box-body">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Message</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($contacts as $contact)
                            <tr>
                                <td><p>{{ $contact->id }}</p></td>
                                <td><p>{{ $contact->fullname }}</p></td>
                                <td><p>{{ $contact->email}}</p></td>
                                <td><p>{{ $contact->subject}}</p></td>
                                <td><p>{{ $contact->message}}</p></td>
                                <td><p>{{ $contact->created_at->format('jS F, Y')}}</p></td>
                            </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </section>
@endsection

@section('scripts')
   <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
   <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
   <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
   <script>
       $('#example1').DataTable({
           "aaSorting": []
       });
   </script> 
@endsection