<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
    <title>Registration | Redeemer's Men Fellowship LP19</title>
</head>
<body>
    <header id="nav">
        <div class="container">
            <nav>
                <div class="nav-brand">
                    <a href="{{route('home')}}">
                        <img src="images/rmf-transparent8.png" alt="RMF Logo" width="160">
                    </a>
                </div>
    
                <!-- Hamburger menu icon -->
                <div class="menu-icons open">
                    <!-- <i class="icon ion-md-menu"></i> -->
                    <i class="ion-android-menu"></i>
                </div>
    
                <ul class="nav-list">
                    <!-- Another Hamburger menu icon -"x" -to close the navigation -->
                    <div class="menu-icons close">
                        <!-- <i class="icon ion-md-close"></i> -->
                        <i class="ion-android-close"></i>
                    </div>
    
                    <li class="nav-item">
                        <a href="{{ route('home') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('registration') }}" class="nav-register">REGISTER</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <main>
        <div class="flyer">
            <img src="{{asset('images/flyer2.jpeg')}}" style="width: 100%; height: auto;" alt="rmf-lp19-convention-flyer">
        </div>

        <div class="container">
            <div id="registration-form">
                @include('partials.errorbag')
                @include('partials.successmsg')
                <h2>Register for the conference</h2>
                <form action="{{ route('registrant.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fullname</label>
                                <input type="text" class="form-control" name="fullname" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Telephone No.</label>
                                <input type="text" class="form-control" name="phone" placeholder="Telephone No." required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>WhatsApp Telephone No.</label>
                                <input type="text" class="form-control" name="whatsapp_phone" placeholder="WhatsApp Telephone No." required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Sex</label>
                                <select class="form-control" name="sex" required>
                                    <option value="">Are you a male or a female?</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Profession</label>
                                <input type="text" class="form-control" name="occupation" placeholder="Profession" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Age Range</label>
                                <select class="form-control" name="age_range" required>
                                    <option value="">Age Range</option>
                                    <option value="Below 30 Years">Below 30 years</option>
                                    <option value="30-40 years">30-40 years</option>
                                    <option value="40-50 years">40-50 years</option>
                                    <option value="Above 50 years">Above 50 years</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>RCCG or Non-RCCG</label>
                                <select class="form-control" name="rccg_non_rccg" required>
                                    <option value="">RCCG or non-RCCG</option>
                                    <option value="RCCG">RCCG</option>
                                    <option value="Non-RCCG">Non-RCCG</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>If RCCG LP19</label>
                                <input type="text" class="form-control" name="if_rccg_lp19" placeholder="Zone, area or Parish">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg">Register</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <section id="contact-us" class="contact-us">
            <div class="container">
                <div class="title-heading">
                    <h1>Contact Us</h1>
                </div>
                <div class="contact-us-container">
                    <div class="contact-text">
                        <div class="description">
                            <p>Representatives of the Redeemer's Men Fellowship can be reached at the parish, area, zone, province and regional level</p>
                        </div>
                        <div class="address">
                            <h3>Address</h3>
                            <p>Strong Tower Sanctuary LP 19 HQ</p>
                            <p>39/41, Ogudu Road, Ojota, Lagos.</p>
                        </div>
                        <div class="phone-email">
                            <h3>Phone/Email</h3>
                            <p><i class="fa fa-phone"></i> 08181920335, 08033068761</p>
                            <p><i class="ion-email"></i> info@rccgrmflp19.org</p>
                        </div>
                        <div class="social-icons">
                            <span><a href="https://web.facebook.com/rccglp19strongtower/" target="_blank"><i class="fa fa-facebook"></i></a></span>
                            <span><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></span>
                            <span><a href="https://www.instagram.com/rccgstrongtowerlp19/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></span>
                            <span><a href="https://www.youtube.com/c/RCCGStrongTowerLP19/featured" target="_blank"><i class="fa fa-youtube"></i></a></span>
                        </div>
                    </div>
                    <div class="contact-form">
                        <div class="header-raised">
                            <h4>Contact Us</h4>
                        </div>
                        <div class="content">
                            <form action="{{ route('contacts.store') }}" method="POST">
                                @csrf
                                <div class="field-container">
                                    <input type="text" placeholder="Your Full Name" name="fullname" required>
                                </div>
                                <div class="field-container">
                                    <input type="email" placeholder="Your Email" name="email" required>
                                </div>
                                <div class="field-container">
                                    <input type="text" placeholder="Subject" name="subject" required>
                                </div>
                                <div class="field-container">
                                    <textarea placeholder="Your Message" name="message" required></textarea>
                                </div>
                                <div class="field-container button">
                                    <button type="submit" class="btn btn-pink">
                                        <div class="overlay1 btn-overlay-pink"></div>
                                            <span><i class="fa fa-paper-plane"></i> SEND MESSAGE</span>
                                        <div class="overlay2 btn-overlay-pink"></div>
                                    </button>
                                </div>
                                <div class="field-container">
                                    @if (session('success'))
                                        <div class="alert alert-success">
                                            <h4><i class="icon fa fa-check"></i> Message sent successfully.</h4>
                                        </div>
                                    @endif

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <div class="container">
            <div class="footer-container">
                <div class="row">
                    <div class="col-md-4 footer-item facebook-feed">
                        <div class="footer-heading">
                            <h5>About Us</h5>
                        </div>
                        <div class="footer-content">
                            <p>Redeemer's Men Fellowship is a Christ Oriented Ministry for men, by men who through the gospel of Jesus Christ emphasize
                            the biblical importance of family, encourage personal growth, and reach out through love, prayer and fellowship.</p>
                        </div>
                    </div>
                    <div class="col-md-4 footer-item facebook-feed">
                        <div class="footer-heading">
                            <h5>Events</h5>
                        </div>
                        <div class="footer-content">
                            <h4>Redeemers Men's Fellowship Convention 2023</h4>
                            <p>Theme : In His Steps</p>
                            <p><i class="fa fa-calendar"></i> &nbsp; Saturday 18th of November, 2023</p>
                            <p><i class="fa fa-clock-o"></i> &nbsp; 10am - 2pm Nigerian Time</p>
                            <p><i class="fa fa-map-marker"></i> &nbsp; Strong Tower Sanctuary LP 19 HQ. 39/41, Ogudu Road, By Victoria B/Stop, Ojota-Lagos</p>
                        </div>
                        {{-- <div class="footer-content">
                            <h4>RMF LP19 Convention 2022</h4>
                            <p>Theme : Prevail 2022</p>
                            <p><i class="fa fa-calendar"></i> &nbsp; Saturday 23rd of July, 2022</p>
                            <p><i class="fa fa-clock-o"></i> &nbsp; 9am - 1pm Nigerian Time</p>
                            <p><i class="fa fa-map-marker"></i> &nbsp; Hybrid (Physical/Virtually)</p>
                        </div> --}}
                    </div>
                    <div class="col-md-4 footer-item facebook-feed">
                        <div class="footer-heading">
                            <h5>Address</h5>
                        </div>
                        <div class="footer-content">
                            <p>Strong Tower Sanctuary LP 19 HQ</p>
                            <p>39/41, Ogudu Road, Ojota, Lagos.</p>
                            <p><i class="fa fa-phone"></i> 08181920335, 08033068761</p>
                            <p><i class="ion-email"></i> info@rccgrmflp19.org</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="bottom-footer">
                <p>&copy; {{date('Y')}} Redeemers Men Fellowship LP19</p>
            </div>
        </div>  
    </footer>

    <script src="{{asset('js/scripts.js')}}"></script>


</body>
</html>
