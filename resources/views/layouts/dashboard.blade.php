<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | {{ config('app.name') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
  
  <link rel="stylesheet" href="{{asset('css/skin-blue.min.css')}}">

  <!-- Datatable -->
  <link rel="stylesheet" href="{{asset('css/dataTables.bootstrap.min.css')}}">

  <style>
    .main-header .logo{
      padding: 0 !important;
    }

    .skin-blue .main-header .logo{
      background: #19221f !important;
    }

    .skin-blue .main-header .navbar{
      background: #9cc61f !important;
    }

    .skin-blue .main-header .navbar .sidebar-toggle:hover{
      background: #498516 !important;
    }
  </style>

  <!-- icheck -->
  <!-- <link rel="stylesheet" href="css/icheck.css"> -->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="https://rccgrmflp19.org/rccgrmflp19/public" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>RMF</b></span>  
      {{-- <span class="logo-mini"><img src="{{asset('images/rmf-transparent8.png')}}" alt="rccg-rmf-lp19-logo"></span>  --}}
      
      <!-- logo for regular state and mobile devices -->
       {{-- <span class="logo-lg"><b>IScholar</b> Initiative</span>  --}}
      <img src="{{asset('images/rmf-transparent8.png')}}" alt="rccg-rmf-lp19-logo" class="img-responsive">
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="{{ route('logout') }}" 
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          {{-- <img src="{{asset('storage/'.Auth::user()->image)}}" class="img-circle" alt="User Image"> --}}
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</p>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li><a href="{{ route('admin.registrants') }}"><i class="fa fa-users"></i> <span>Registrants</span></a>
        <li><a href="{{ route('admin.contacts') }}"><i class="fa fa-users"></i> <span>Contacts</span></a>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')

     
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="pull-right hidden-xs">
        <!-- Anything you want -->
      </div>

      <!-- Default to the left -->
      <strong>&copy; {{ date('Y') }} {{ config('app.name') }}.</strong> All rights reserved.
    </footer>

  <!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/adminlte.min.js')}}"></script>
<!-- Datatables -->
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>

@yield('scripts')

</body>
</html>