<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registrant extends Model
{
    //
    protected $fillable = ['fullname', 'phone', 'whatsapp_phone', 'email', 'age_range', 'occupation', 'rccg_non_rccg', 'if_rccg_lp19'];
}
