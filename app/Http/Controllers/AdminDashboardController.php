<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registrant;
use App\Contact;

class AdminDashboardController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function showDashboard(){
       $registrants = Registrant::latest()->get();

       return view('admindashboard.registrants', compact('registrants'));
    }

    public function showRegistrants(){
       $registrants = Registrant::latest()->get();

       return view('admindashboard.registrants', compact('registrants'));
    }

    public function showContacts(){
       $contacts = Contact::latest()->get();

       return view('admindashboard.contacts', compact('contacts'));
    }
}
