<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registrant;

class RegistrantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        //Validate
        $this->validate(request(), [
            'fullname'=>'required',
            'phone'=>'required',
            'whatsapp_phone'=>'required',
            'email'=>'required|email|unique:registrants',
            'age_range'=>'required',
            'occupation'=>'required',
            'rccg_non_rccg'=>'required',
            // 'if_rccg_lp19'=>'required',
        ],[
            'email.unique'=>'You have already registered for this conference.'
        ]);

        Registrant::create([
            'fullname'=>$request->fullname,
            'phone'=>$request->phone,
            'whatsapp_phone'=>$request->whatsapp_phone,
            'email'=>$request->email,
            'age_range'=>$request->age_range,
            'occupation'=>$request->occupation,
            'rccg_non_rccg'=>$request->rccg_non_rccg,
            'if_rccg_lp19'=>$request->if_rccg_lp19,
        ]);

        return redirect('/registration#registration-form')->with('success', 'Registration successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
