<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/migrate', function () {
    Artisan::call('migrate');
    return 'Migration OK';
});

Route::get('/migratefresh', function () {
    Artisan::call('migrate:fresh');
    return 'Migrate fresh OK';
});

Route::get('/seeder', function(){
    Artisan::call('db:seed');

    return 'Seeder OK';
});

Auth::routes(['register'=>false]);

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/registration', function () {
    return view('registration');
})->name('registration');

Route::post('registrant', 'RegistrantController@store')->name('registrant.store');

Route::post('contacts', 'ContactController@store')->name('contacts.store');

Route::get('admin/dashboard', 'AdminDashboardController@showDashboard')->name('admin.dashboard');

Route::get('admin/registrants', 'AdminDashboardController@showRegistrants')->name('admin.registrants');

Route::get('admin/contacts', 'AdminDashboardController@showContacts')->name('admin.contacts');
